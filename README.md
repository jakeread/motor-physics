# PMSM Minor Motor Physics

Brushless DC (BLDC) motors, aka Permanent Magnet Synchronous Motors (PMSM), are common wherever we would like to have highly controllable force (and so motion) output.

PMSM originated in the 80's and became ubiquitous as the cost of the computing that is needed to control them has dropped. As opposed to Brushed DC Motors that rely on mechanical switching to change coil currents, PMSM require artificial switching for commutation.

![drv](ref/pancake.jpg)

```
'magnets', how do they work?
  - ICP
```

I wanted to make an attempt to illuminate how PMSM motors work. I'm going to do this with COMSOL, because it's a familiar tool.

![drv](ref/bldc-motor-gif.gif)

First, a rotor and stator, that I'm going to lay out as if it were planar (we can take for granted that these 'wrap' into rotary motors).

## Cogging Torque

Ok, first, I'd like to see if I can plot cogging torque. This is the affect that the permanent magnets have on the iron core, without driving any current to induce a field in the core.

Ok, this is pretty neat. Forgive my improperly-periodical COMSOL simulation, but we can see this pretty well:

**0 Amps**

![cogging](cogging-03-anim.gif)

Here's the force in the x-direction on the strip of magnets:

![cogplot](cogging-03-plot.png)

We can see that periodic force between -10N and +10N.

## Driving Torque

I'm not about to commutate this simulation, but we can compare the above against some driven current. With BLDC, one of the simplest (and effective at high speeds) ways to drive is so choose two of the three phases, and drive through those. I'll wrap a coil around my 1st and 2nd phase, and drive a field through them (this way they will 'mimic' a N and S permanent magnet).

**1 Amp on A* and B_**

![driven](driven-01-anim.gif)

We can see now two peaks, rather than our three from the cogging torque: this is because we have biased the field towards two of the three phases:

![drivenplot](driven-01-plot.png)

Hopefully this demonstrates the principle. We can see that we've greatly biased the x-component (torque) force during a particular part of the phase. We can imagine that by moving this point in the phase plot, we can commutate.

## Reluctance

So then, per Neil's request, what the heck is a reluctance motor?

![reluctance](ref/high-and-low-reluctance.jpg)
![reluctance](ref/variable-reluctance-motor.jpg)

In practice, these take on interesting geometries:

![rel](ref/reluctance-in-practice.jpg)
[ref](https://www.ee.co.za/article/advantages-synchronous-reluctance-motor.html)

The idea is straightforward: the rotor, being of some magnetic material having some BH curve that is more favourable to a field than open space, is set up to have positions in mechanical phase where the rotor-irons line up with the stator-irons. When coils are excited, the field attempts to minimize energy by pulling the rotor into these positions. By switching through a series of these points, we can commutate the motor.

I've modified my COMSOL model here to eliminate permanent magnets, instead just a similar 'reciprocal' iron structure. We can see that cogging torque doesn't exist here, because there's no field to begin with.

![red-dead-reluctance](reluctance-00-anim.gif)

But if we drive some of the phases, there are a few occasions where the rotor and stator alignment favours others:

![reluctance](reluctance-02-anim.gif)

and this generates *some* torque:

![reluctplot](reluctance-02-plot.png)

## Further Work

I'm dropping this off at 5/24/2019, but I will likely return. 

questions I have about motors

 - electric power output has hard limits based on physics: what are these limits and which physics are they a result of ? where are their bounds ?
 - motor aspect ratios change motor landing point in motor parameter space, how do we describe these relationships, those parameter spaces ?
